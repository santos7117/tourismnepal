# Steps :-

1. Clone this repo
2. Download & install XAMPP & set path
3. Start MySQL & Apache servers
4. Import database from travelnepal.sql file
5. Copy downloaded repo to htdocs folder inside XAMP installation path
6. Run localhost/tourismnepal/client for frontend
7. Run localhost/tourismnepal/admin for admin panel

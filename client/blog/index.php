<!DOCTYPE html>
<html>
    <?php require_once('../views/head.php'); ?>
    <?php require_once('../../config.php'); ?>
    <body>
    <?php require_once('../views/navbar.php'); ?>
        <!-- header -->
        <header class = "flex header-sm">
            <div class = "container">
                <div class = "header-title">
                    <h1>Blog</h1>
                </div>
            </div>
        </header>
        <!-- header -->

        <!-- blog section -->
        <section id = "blog" class = "py-4">
            <div class = "container">
                <div class = "title-wrap">
                    <h3 class = "lg-title">recent blogs</h3>
                </div>

                <div class = "blog-row">
                <?php
                $qry = $conn->query("SELECT * FROM `blogs` ORDER BY DATE(date_updated) DESC");
                while($row = $qry->fetch_assoc()) :?>
                <div class = "blog-item my-2 shadow">
                    <div class = "blog-item-top">
                        <img src = "../views/images/blog-1.jpg" alt = "blog">
                        <span class = "blog-date"><?php echo $row['date_updated']; ?></span>
                    </div>
                    <div class = "blog-item-bottom">
                        <p class = "text"><?php echo $row['blog_text']; ?></p>
                    </div>
                    </div>
                <?php endwhile; ?>
                </div>
        </section>
        <!-- end of blog section -->
        

        <?php require_once('../views/footer.php'); ?>

        <!-- js -->
        <script src = "js/script.js"></script>
        <script>
            
        </script>
    </body>
</html>
<!-- navbar  -->
<nav class = "navbar">
    <div class = "container flex">
        <a href = "index.html" class = "site-brand">
            Travel<span>Nepal</span>
        </a>

        <button type = "button" id = "navbar-show-btn" class = "flex">
            <i class = "fas fa-bars"></i>
        </button>
        <div id = "navbar-collapse">
            <button type = "button" id = "navbar-close-btn" class = "flex">
                <i class = "fas fa-times"></i>
            </button>
            <ul class = "navbar-nav">
                <li class = "nav-item">
                    <a href = "/tourismnepal/client" class = "nav-link">Home</a>
                </li>
                <li class = "nav-item">
                    <a href = "/tourismnepal/client/gallery" class = "nav-link">Gallery</a>
                </li>
                <li class = "nav-item">
                    <a href = "/tourismnepal/client/blog" class = "nav-link">Blog</a>
                </li>
                <li class = "nav-item">
                    <a href = "/tourismnepal/client/views/about.html" class = "nav-link">About</a>
                </li>
                <li class = "nav-item">
                    <a href = "/tourismnepal/client/views/contact.html" class = "nav-link">Contact</a>
                </li>
                <li class = "nav-item">
                    <a href = "/tourismnepal/client/login" class = "nav-link">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- end of navbar  -->
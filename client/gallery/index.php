<!DOCTYPE html>
<html>
  <?php require_once('../views/head.php'); ?>
  <?php require_once('../../config.php'); ?>
  <body>
    <?php require_once('../views/navbar.php'); ?>

    <!-- header -->
    <header class="flex header-sm">
      <div class="container">
        <div class="header-title">
          <h1>Gallery</h1>
        </div>
      </div>
    </header>
    <!-- header -->

    <!-- gallery section -->
    <div id="gallery" class="py-4">
      <div class="container">

      <div class="gallery-row">
            <?php
              $qry = $conn->query("SELECT * FROM `packages` ORDER BY DATE(date_created) DESC");
              while($row = $qry->fetch_assoc()) :
                $row['description'] = strip_tags(stripslashes(html_entity_decode($row['description'])));
            ?>
          <div class = "blog-item my-2 shadow">
                        <div class = "blog-item-top">
                            <img src = "../views/images/gallery8.jpg">
                            <span class = "blog-date"><?php echo $row['date_created']; ?></span>
                        </div>
                        <div class = "blog-item-bottom">
                            <span><?php echo $row['tour_location']; ?></span>
                            <a href = "#"><?php echo $row['title']; ?></a>
                            <p class = "text"><?php echo $row['description']; ?></p>
                        </div>
                    </div>
					<?php endwhile; ?>
        </div>
      </div>
    </div>
    <!-- end of gallery section -->

    <!-- img modal -->
    <div id="img-modal-box">
      <div id="img-modal">
        <button type="button" id="modal-close-btn" class="flex">
          <i class="fas fa-times"></i>
        </button>
        <button type="button" id="prev-btn" class="flex">
          <i class="fas fa-chevron-left"></i>
        </button>
        <button type="button" id="next-btn" class="flex">
          <i class="fas fa-chevron-right"></i>
        </button>
        <img src="images/gallery-1.jpg" />
      </div>
    </div>
    <!-- end of img modal -->

    <?php require_once('../views/footer.php'); ?>

    <!-- js -->
    <script src="js/script.js"></script>
    <script>
      // image modal
      const allGalleryItem = document.querySelectorAll(".gallery-item");
      const imgModalDiv = document.getElementById("img-modal-box");
      const modalCloseBtn = document.getElementById("modal-close-btn");
      const nextBtn = document.getElementById("next-btn");
      const prevBtn = document.getElementById("prev-btn");
      let imgIndex = 0;

      allGalleryItem.forEach(galleryItem => {
        galleryItem.addEventListener("click", () => {
          imgModalDiv.style.display = "block";
          let imgSrc = galleryItem.querySelector("img").src;
          imgIndex = parseInt(imgSrc.split("-")[1].substring(0, 1));
          showImageContent(imgIndex);
        });
      });

      // next click
      nextBtn.addEventListener("click", () => {
        imgIndex++;
        if (imgIndex > allGalleryItem.length) {
          imgIndex = 1;
        }
        showImageContent(imgIndex);
      });

      // previous click
      prevBtn.addEventListener("click", () => {
        imgIndex--;
        if (imgIndex <= 0) {
          imgIndex = allGalleryItem.length;
        }
        showImageContent(imgIndex);
      });

      function showImageContent(index) {
        imgModalDiv.querySelector(
          "#img-modal img"
        ).src = `images/gallery-${index}.jpg`;
      }

      modalCloseBtn.addEventListener("click", () => {
        imgModalDiv.style.display = "none";
      });
    </script>
  </body>
</html>
